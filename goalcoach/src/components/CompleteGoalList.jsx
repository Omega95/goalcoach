import React, { Component } from 'react';
import { completeGoalRef } from '../firebase';
import { setCompleted } from '../actions';
import { connect } from 'react-redux';

class CompleteGoalList extends Component {

  componentDidMount() {

      completeGoalRef.on('value', snap => {
        let completeGoals = [];
        snap.forEach (completeGoal => {
          const {email, title} = completeGoal.val();
          completeGoals.push({email, title})
        })
        console.log('completeGoals', completeGoals)
        this.props.setCompleted(completeGoals);
      })
  }

  render() {
    return (
      <div>
        {
        
        }
        <button
          className="btn btn-primary"
        >
          Clear All
        </button>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const {completeGoals} = state;
  return {
    completeGoals
  }
}

export default connect(null, {setCompleted})(CompleteGoalList);
