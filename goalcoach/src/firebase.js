import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyBxiDnIoEKbNXaREpEqcK6l1IPwXTMHfUQ",
  authDomain: "goalcoach-49a17.firebaseapp.com",
  databaseURL: "https://goalcoach-49a17.firebaseio.com",
  projectId: "goalcoach-49a17",
  storageBucket: "",
  messagingSenderId: "1021741668077"
};

export const firebaseApp = firebase.initializeApp(config);
export const goalRef = firebase.database().ref('goals');
export const completeGoalRef = firebase.database().ref('completeGoals')
